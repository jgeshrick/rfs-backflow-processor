﻿
using RFS_Backflow_Processor.Common;
//using RFS_Backflow_Processor.Interfaces;
using RFS_Backflow_Processor.Models;
using RestSharp;
using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using Xamarin.Forms;

namespace RFS_Backflow_Processor.Services
{
	public class NTGApiResult
	{
		public bool Success { get; set; } = true;
	}

	public class NFPProcessFileRequest
	{

		/// <summary>
		/// 
		/// </summary>
		public string SiteId { get; set; } = default;

		/// <summary>
		/// 
		/// </summary>
		public string FileType { get; set; } = default;

	}

	public class ApiServiceBase
	{
		//protected ILogger Log = DependencyService.Get<ILogManager>().GetLog();

		Func<string> _baseUrl = null;

		protected const int _20_MINUTES = (60 * 20 * 1000);
		protected const int _10_MINUTES = (60 * 10 * 1000);
		protected const int _2_MINUTES = (60 * 2 * 1000);
		protected const int _1_MINUTE = (60 * 1 * 1000);
		protected const int _30_SECONDS = (30 * 1 * 1000);

		public bool IsInitialized { get; private set; } = false;

		public ApiServiceBase() { }

		public void Initialize(Func<string> baseUrl)
		{
			this._baseUrl = baseUrl;
			var s = ValidateRequiredMembers();
			if (!string.IsNullOrEmpty(s))
			{
				//Log.Error(s);
				throw new Exception(s);
			}
			this.IsInitialized = true;
		}

		string ValidateRequiredMembers()
		{
			if (this._baseUrl == null)
				return "Remote Url Func<> is Required";
			return string.Empty;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="restPath"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="baseUrl"></param>
        /// <param name="timeOut"></param>
        /// <returns></returns>
        public async Task<TValue> GetAsync<TValue>(string restPath, CancellationToken cancellationToken, int? timeOut)
        {

            string baseUrl = this._baseUrl();


            TimeSpan ts = new TimeSpan(0, 0, 0, 0, timeOut ?? _2_MINUTES);

            var request = new RestRequest(restPath, Method.GET)
            {
                OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; },
                JsonSerializer = new RestSharp.Serialization.Json.JsonSerializer()
            };

            request.AddHeader("Authorization", $"Bearer {Program.AuthService.AuthToken}");
			request.Timeout = (int)ts.TotalMilliseconds;
            RestClient rc = new RestClient(baseUrl);

            Console.WriteLine(baseUrl + restPath);

            try
            {
				//var respnse = await rc.ExecuteAsync(request, cancellationToken);

                IRestResponse<TValue> restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);

                if (restResponse.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var res = await Program.AuthService.RefreshTokenAsync();
					if (res == Enumerations.EnHostCommunicationResult.success)
					{
						restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);
						if (restResponse.StatusCode == HttpStatusCode.OK)
						{
							return restResponse.Data;
						}
					}
					else
					{
						var b = await Program.AuthService.AuthenticateUser(Program.user, Program.pass);
						if (b)
						{
							restResponse = await rc.ExecuteAsync<TValue>(request, cancellationToken);
							if (restResponse.StatusCode == HttpStatusCode.OK)
							{
								return restResponse.Data;
							}
						}

					}
                }
                else if (restResponse.StatusCode == HttpStatusCode.OK)
                {
                    return restResponse.Data;
                }

            }
            catch (Exception _ex)
            {
                Console.WriteLine(_ex.TraceSummary());
                throw _ex;
            }

            return default(TValue);
        }




        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <typeparam name="TRequestData"></typeparam>
        /// <param name="restPath"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="baseUrl"></param>
        /// <param name="timeOut"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<TValue> PutAsync<TValue, TRequestData>(string restPath, CancellationToken cancellationToken, int? timeOut, TRequestData requestData = default(TRequestData))
		{
			try
			{
				string baseUrl = this._baseUrl();

				Console.WriteLine($"PUT: BaseURL:[{baseUrl}], Api:[{restPath}]");

				if (string.IsNullOrEmpty(Program.AuthService?.AuthToken))
				{
					System.Console.WriteLine("No Auth Token");
					return default(TValue);
				}
                //TimeSpan ts = new TimeSpan(0, 0, 0, 0, timeOut ?? _2_MINUTES);

                var request = new RestRequest(restPath, Method.POST)
                {
                    OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; },
                    RequestFormat = DataFormat.Json
                };

                request.AddJsonBody(requestData);
				request.AddHeader("Authorization", $"Bearer {Program.AuthService.AuthToken}");
				//request.Timeout = ts.Milliseconds;

				RestClient rc = new RestClient(baseUrl);


				IRestResponse<TValue> restResponse = await rc.ExecuteAsync<TValue>(request);

				if (restResponse.StatusCode == HttpStatusCode.Unauthorized)
				{
					var res = Program.AuthService.RefreshTokenAsync().Result;
					if (res == Enumerations.EnHostCommunicationResult.success)
					{
						restResponse = rc.Execute<TValue>(request);
						if (restResponse.StatusCode == HttpStatusCode.OK)
						{
							return restResponse.Data;
						}
					}
				}
				else if (restResponse.StatusCode == HttpStatusCode.OK)
				{
					return restResponse.Data;
				}
			}
			catch (Exception _ex)
			{
				Console.WriteLine($"Exception: {_ex.Message}");
				//Log.Error(_ex.TraceSummary());
				throw _ex;
			}
			return default(TValue);
		}
	}
}
