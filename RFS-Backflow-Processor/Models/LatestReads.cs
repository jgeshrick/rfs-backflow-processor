﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace RFS_Backflow_Processor.Models
{
    [NotMapped]
    public class LatestReadingApiRequest
    {
        [JsonProperty(PropertyName = "begin_date")]
        public DateTime BeginDate { get; set; }

        [JsonProperty(PropertyName = "end_date")]
        public DateTime EndDate { get; set; }

        [JsonProperty(PropertyName = "request_details")]
        public RequestDetail[] RequestDetails { get; set; }
    }

    public class RequestDetail
    {
        [JsonProperty(PropertyName = "site_id")]
        public string SiteId { get; set; }

        [JsonProperty(PropertyName = "miu_id")]
        public string CollectionId { get; set; }

        [JsonProperty(PropertyName = "meter_number")]
        public string MeterNumber { get; set; }

        [JsonProperty(PropertyName = "rodb_unique_id")]
        public string RodbUniqueId { get; set; }
    }

    public class LatestReadingApiResponse
    {
        [JsonProperty(PropertyName = "miu_id")]
        public string CollectionId { get; set; }

        [JsonProperty(PropertyName = "meter_number")]
        public string MeterNumber { get; set; }

        [JsonProperty(PropertyName = "rodb_unique_id")]
        public string RodbUniqueId { get; set; }

        [JsonProperty(PropertyName = "site_id")]
        public string SiteId { get; set; }

        [JsonProperty(PropertyName = "reading")]
        public string Reading { get; set; }

        [JsonProperty(PropertyName = "reading_datetime")]
        public string ReadingDatetime { get; set; }

        [JsonProperty(PropertyName = "device_type")]
        public string DeviceType { get; set; }

        [JsonProperty(PropertyName = "formatted_reading")]
        public string FormattedReading { get; set; }

        [JsonProperty(PropertyName = "current_leak")]
        public int CurrentLeak { get; set; }

        [JsonProperty(PropertyName = "leak_35_days")]
        public int Leak35Days { get; set; }

        [JsonProperty(PropertyName = "water_no_flow_35_days")]
        public int WaterNoFlow35Days { get; set; }

        [JsonProperty(PropertyName = "peak_back_flow")]
        public int PeakBackFlow { get; set; }
    }
}
