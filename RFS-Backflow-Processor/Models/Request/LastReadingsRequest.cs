﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFS_Backflow_Processor.Models.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class LastReadingsRequest
    {
        public bool exclude_invalid_readings { get; set; } = false;
        public List<LastReadingsRequestEndpoint> endpoints { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LastReadingsRequestEndpoint
    {
        public string site_id { get; set; }
        public string miu_id { get; set; }
        public string meter_number { get; set; }
    }
}
