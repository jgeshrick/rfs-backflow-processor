﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFS_Backflow_Processor.Models.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadingsRequest
    {
        public string begin_date { get; set; }
        public string end_date { get; set; }
        public List<ReadingsRequestEndpoint> endpoints { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReadingsRequestEndpoint
    {
        public string site_id { get; set; }
        public string miu_id { get; set; }
        public string meter_number { get; set; }
    }
}
