﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFS_Backflow_Processor.Models.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class EndpointsRequest
    {
        public List<EndpointsRequestEndpoint> endpoints { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EndpointsRequestEndpoint
    {
        public string site_id { get; set; }
        public string miu_id { get; set; }
        public string meter_number { get; set; }
    }
}
