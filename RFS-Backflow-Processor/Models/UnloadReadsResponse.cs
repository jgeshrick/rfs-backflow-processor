﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RFS_Backflow_Processor.Models
{
    public class UnloadReadsResponse
    {
        public int status { get; set; } = 1;
    }
}
