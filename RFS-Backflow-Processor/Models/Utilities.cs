﻿using System;
namespace RFS_Backflow_Processor.Models
{
    public class UtilityResponse
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int UtilityId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string UtilityName { get; set; }

        /// <summary>
        /// Gets or sets the Enabled.
        /// </summary>
        /// <value>
        /// The Enabled.
        /// </value>
        public Boolean IsActive { get; set; }

        /// <summary>
        /// Gets or sets the parent utility.
        /// </summary>
        /// <value>
        /// The parent utility.
        /// </value>
        public int ParentUtilityId { get; set; }

        /// <summary>
        /// Gets or sets the country code identifier.
        /// </summary>
        /// <value>
        /// The country code identifier.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is DST enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is DST enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsDstEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the preferred timezone.
        /// </summary>
        /// <value>
        ///   The timezone identifier
        /// </value>
        public string TimeZone { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the timestamp id.
        /// </summary>
        /// <value>
        ///   The timestampid identifier
        /// </value>
        public int TimestampId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance will Apply UTC Offset.
        /// </summary>
        /// <value>
        ///   The ApplyUTCOffset identifier
        /// </value>
        public bool ApplyUTCOffset { get; set; }

        /// <summary>
        /// Gets or sets the site id identifier.
        /// </summary>
        /// <value>
        /// The site id identifier.
        /// </value>
        public string SiteId { get; set; }

        /// <summary>
        /// Gets or sets the address1.
        /// </summary>
        /// <value>
        /// The address1.
        /// </value>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        /// <value>
        /// The address2.
        /// </value>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        /// <value>
        /// The zip.
        /// </value>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets the trasmit fequency.
        /// </summary>
        /// <value>
        /// The trasmit fequency.
        /// </value>
        public decimal TransmitFrequency { get; set; }

        /// <summary>
        /// Gets or sets the receive frequency.
        /// </summary>
        /// <value>
        /// The receive frequency.
        /// </value>
        public decimal ReceiveFrequency { get; set; }

        /// <summary>
        /// Gets or sets the system identifier.
        /// </summary>
        /// <value>
        /// The system identifier.
        /// </value>
        public int SystemId { get; set; }

        /// <summary>
        /// Gets or sets the Utility Latitude.
        /// </summary>
        /// <value>
        /// The Latitude.
        /// </value>
        public decimal Latitude { get; set; }

        /// <summary>
        /// Gets or sets the Utility Longitude.
        /// </summary>
        /// <value>
        /// The longitude.
        /// </value>
        public decimal Longitude { get; set; }

        /// <summary>
        /// Gets or sets the Utility default UOM.
        /// </summary>
        /// <value>
        /// The UOM.
        /// </value>
        public string UOM { get; set; }

        /// <summary>
        /// Gets or sets the Utility default ImportFileVersion.
        /// </summary>
        /// <value>
        /// The ImportFileVersion.
        /// </value>
        public string ImportFileVersion { get; set; }

        /// <summary>
        /// Gets or sets the Utility default close on Export setting.
        /// </summary>
        /// <value>
        /// The CloseOnExport.
        /// </value>
        public bool CloseOnExport { get; set; }

        /// <summary>
        /// Gets or sets Utility Default Auto-Complete Days
        /// </summary>
        /// <value>
        /// The AutoCompleteDays.
        /// </value>
        public int AutoCompleteDays { get; set; }

        /// <summary>
        /// Gets or sets the Mobile Mapping setting
        /// </summary>
        /// <value>
        /// The MobileMapping.
        /// </value>
        public bool MobileMapping { get; set; }

        /// <summary>
        /// Gets or sets the Handheld Support setting
        /// </summary>
        /// <value>
        /// The HandheldSupport.
        /// </value>
        public bool HandheldSupport { get; set; }

        /// <summary>
        /// Gets or sets the Handheld Eula setting
        /// </summary>
        /// <value>
        /// The HandheldEulaAccepted.
        /// </value>
        public bool HandheldEulaAccepted { get; set; }

        /// <summary>
        /// Gets or sets the Handheld Eula user setting
        /// </summary>
        /// <value>
        /// The HandheldEulaUserId.
        /// </value>
        public int? HandheldEulaUserId { get; set; }

        /// <summary>
        /// Gets or sets the Data Retention Years configured for the Utility
        /// </summary>
        /// <value>
        /// Utility's Data Retention Period in Years
        /// </value>
        public int DataRetentionYears { get; set; }

        /// <summary>
        /// Gets or sets the Oldest Data Point Date for the Utility
        /// </summary>
        /// <value>
        /// Utility's Oldest Data Point Date
        /// </value>
        public DateTime OldestDataPointDate { get; set; }
        public bool ImportByLine { get; set; }
        public bool DataTranslation { get; set; }
    }

    public class UserName
    {
        public string Username { get; set; }
    }
}
