﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFS_Backflow_Processor.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public class EndpointsResponse
    {
        public List<EndpointsResponseEndpoint> endpoints { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PagedEndpointsResponse : PagedResponse
    {
        public List<EndpointsResponseEndpoint> endpoints { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EndpointsResponseEndpoint
    {
        public string site_id { get; set; }
        public string account_number { get; set; }
        public string premise_key { get; set; }
        public string miu_id { get; set; }
        public string register_id { get; set; }
        public string meter_number { get; set; }
        public string meter_type { get; set; }
        public string meter_size { get; set; }
        public string meter_manufacturer { get; set; }
        public string dials { get; set; }
        public string multiplier { get; set; }
        public string unit_of_measure { get; set; }
    }
}
