﻿//using DS.Models.Shadow.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFS_Backflow_Processor.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadingsResponse
    {
        public List<ReadingsResponseEndpoint> endpoints { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PagedReadingsResponse : PagedResponse
    {
        public List<ReadingsResponseEndpoint> endpoints { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReadingsResponseEndpoint
    {
        public string site_id { get; set; }
        public string miu_id { get; set; }
        public string meter_number { get; set; }
        public List<ReadingsResponseEndpointReading> readings { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReadingsResponseEndpointReading
    {
        public string reading { get; set; }
        public DateTime reading_datetime { get; set; }
        public byte consumption_state { get; set; }
        public byte consumption_35_days { get; set; }
        public byte reverse_flow_state { get; set; }
        public byte no_flow_35_days { get; set; }
    }
}
