﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RFS_Backflow_Processor.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PagedResponse
    {
        public Paging paging { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Paging
    {
        public int page { get; set; }
        public int limit { get; set; }
        public int total { get; set; }

        public string next { get; set; }
        public string prev { get; set; }
        public string self { get; set; }
    }
}
