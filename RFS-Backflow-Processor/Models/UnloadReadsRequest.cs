﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RFS_Backflow_Processor.Models
{
    /// <summary>
    /// Device types
    /// </summary>
    public enum eDeviceType
    {
        /// <summary>
        /// undefined device type
        /// </summary>
        undefined,

        /// <summary>
        /// SURF MIU
        /// </summary>
        surf,       // SURF

        /// <summary>
        /// SCM/SCM+ MIU
        /// </summary>
        scm,        // SCM/SCM+

        /// <summary>
        /// Advantage MIU
        /// </summary>
        advantage   // Advantage
    };

    public enum eIndustryType
    {
        /// <summary>
        /// undefined industry type
        /// </summary>
        undefined = -1,

        /// <summary>
        /// water
        /// </summary>
        water = 0,

        /// <summary>
        /// gas
        /// </summary>
        gas,

        /// <summary>
        /// electric
        /// </summary>
        electric,
    };


    /// <summary>
    /// 
    /// </summary>
    public class UnloadReadsRequest
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="UnloadReadsRequest"/> is synchronous.
        /// </summary>
        /// <value>
        ///   <c>true</c> if synchronous; otherwise, <c>false</c>.
        /// </value>
        public bool Synchronous { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsTrimbleReading { get; set; } = false;

        /// <summary>
        /// Gets or sets the readings.
        /// </summary>
        /// <value>
        /// The readings.
        /// </value>
        public List<ReadingRequest> Readings { get; set; } = new List<ReadingRequest>();
    }

    public class ReadingRequest
    {
        /// <summary>
        /// Gets or sets the order key.
        /// </summary>
        /// <value>
        /// The order key.
        /// </value>
        public string OrderKey { get; set; }

        /// <summary>
        /// Gets or sets the type of the device.
        /// </summary>
        /// <value>
        /// The type of the device.
        /// </value>
        public eDeviceType DeviceType { get; set; }

        /// <summary>
        /// Gets or sets the type of the industry.
        /// </summary>
        /// <value>
        /// The type of the industry.
        /// </value>
        public eIndustryType IndustryType { get; set; }

        /// <summary>
        /// Gets or sets the miuid.
        /// </summary>
        /// <value>
        /// The miuid.
        /// </value>
        public string MIUID { get; set; }

        /// <summary>
        /// Gets or sets the type of the meter.
        /// </summary>
        /// <value>
        /// The type of the meter.
        /// </value>
        public string MeterType { get; set; }

        /// <summary>
        /// Gets or sets the type of the read.
        /// </summary>
        /// <value>
        /// The type of the read.
        /// </value>
        public string ReadType { get; set; }

        /// <summary>
        /// Gets or sets the display dials.
        /// </summary>
        /// <value>
        /// The display dials.
        /// </value>
        public int? DisplayDials { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        public long? Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the reading packet.
        /// </summary>
        /// <value>
        /// The reading packet.
        /// </value>
        public string ReadingPacket { get; set; }

        /// <summary>
        /// Gets or sets the manual reading.
        /// </summary>
        /// <value>
        /// The manual reading.
        /// </value>
        public long? ManualReading { get; set; }

        /// <summary>
        /// Gets or sets the skip code.
        /// </summary>
        /// <value>
        /// The skip code.
        /// </value>
        public string SkipCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CommentCode1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CommentCode2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NoteBack { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ReadCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ReaderId { get; set; } = string.Empty;

        /// <summary>
        /// Reentry Count 
        /// </summary>
        public int ReentryCount { get; set; } = 0;
    }
}
