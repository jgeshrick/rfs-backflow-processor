﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace RFS_Backflow_Processor.Models
{
    public class Endpoint
    {
        [JsonProperty("site_id")]
        public string site_id { get; set; }

        [JsonProperty("account_number")]
        public string account_number { get; set; }

        [JsonProperty("premise_key")]
        public string premise_key { get; set; }

        [JsonProperty("miu_id")]
        public string miu_id { get; set; }

        [JsonProperty("register_id")]
        public string register_id { get; set; }

        [JsonProperty("meter_number")]
        public string meter_number { get; set; }

        [JsonProperty("meter_type")]
        public string meter_type { get; set; }

        [JsonProperty("meter_size")]
        public string meter_size { get; set; }

        [JsonProperty("meter_manufacturer")]
        public string meter_manufacturer { get; set; }

        [JsonProperty("dials")]
        public string dials { get; set; }

        [JsonProperty("multiplier")]
        public string multiplier { get; set; }

        [JsonProperty("unit_of_measure")]
        public string unit_of_measure { get; set; }
    }

    public class Paging
    {
        [JsonProperty("page")]
        public int page { get; set; }

        [JsonProperty("limit")]
        public int limit { get; set; }

        [JsonProperty("total")]
        public int total { get; set; }

        [JsonProperty("next")]
        public string next { get; set; }

        [JsonProperty("prev")]
        public string prev { get; set; }

        [JsonProperty("self")]
        public string self { get; set; }
    }

    public class EndpointResponse
    {
        [JsonProperty("endpoints")]
        public List<Endpoint> endpoints { get; set; } = new List<Endpoint>();

        [JsonProperty("paging")]
        public Paging paging { get; set; }
    }
}
