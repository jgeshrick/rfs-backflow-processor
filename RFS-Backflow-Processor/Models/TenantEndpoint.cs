﻿using System;
namespace RFS_Backflow_Processor.Models
{
    public class TenantEndpoint
    {
        public string RouteId { get; set; } = "";
        public string AccountNumber { get; set; } = "";
        public string AccountStatus { get; set; } = "";
        public string AccountName { get; set; } = "";
        public string AddressStreet { get; set; } = "";
        public string MeterSize { get; set; } = "";
        public string MeterNumber { get; set; } = "";
        public string MIUID { get; set; } = "";
    }
}
