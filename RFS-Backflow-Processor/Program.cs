﻿using System;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using __AuthService = RFS_Backflow_Processor.Services.AuthService;

namespace RFS_Backflow_Processor
{
    class Program
    {
        static MySqlConnection _mySqlConnection = null;

        static string URI = Common.Strings.N360Domains.USA.DevShadow;
        static string API_IMPEXP_GETACCOUNTDETAILS = "api/ImportExport/comingsoon/";
        static string API_GETUTILITIES = "api/v2/utilities";

        //this is going to change soon. new endpoint coming soon
        static string API_GET_SHADOW_READINGS_FOR_SITE = "api/v2/readings?site_id={0}&begin_date={1}&end_date={2}&page={3}";
        //static string API_SHADOW_READINGS = "api/v2/readings";
        //static string API_SHADOW_LATESTREAD = "api/v1/latestreading";
        //static string API_SHADOW_GETENDPOINTS = "api/v1/endpoints?site_id={0}&page=1";

        static string SITEID = "12345";
        static string IDP = "http://idp.dev.neptune360.com";


        public static string user = "neptune@domain.com";
        public static string pass = "Neptune123";

        static Services.ApiServiceBase ApiService;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Program.ApiService = new Services.ApiServiceBase();
            Program.ApiService.Initialize(Program.ApiBaseUrl);

            var b = Program.AuthService.AuthenticateUser(Program.user, Program.pass).Result;

            var token = new System.Threading.CancellationTokenSource().Token;

            DateTime dtStart = DateTime.UtcNow.AddDays(-729);

            using (_mySqlConnection = new MySqlConnection(Common.Strings.RDSConnections.DEV_EVENTS))
            {
                _mySqlConnection.Open();

                while (dtStart < DateTime.UtcNow)
                {
                    dtStart = dtStart.AddDays(1);

                    Models.Response.Paging page = new Models.Response.Paging();
                    string apiRequest = string.Format(API_GET_SHADOW_READINGS_FOR_SITE,
                        SITEID,
                        dtStart.ToString("yyyy-MM-dd"),
                        dtStart.AddDays(1).ToString("yyyy-MM-dd"),
                        "1");

                    Console.WriteLine(apiRequest);

                    do
                    {
                        page = GetAllReadingsByDate(apiRequest).Result;
                        apiRequest = Common.Strings.N360Domains.USA.DevShadow + page.next;
                    } while (!string.IsNullOrEmpty(page.next));
                }
            }
            // wait - not to end
            new System.Threading.AutoResetEvent(false).WaitOne();
        }

        public static async Task<Models.Response.Paging> GetAllReadingsByDate(string apiRequest)
        {
            Models.Response.Paging retVal = new Models.Response.Paging();

            try
            {
                var token = new System.Threading.CancellationTokenSource().Token;

                var response = await Program.ApiService.GetAsync<Models.Response.PagedReadingsResponse>(
                    apiRequest, token, null);


                if (response != null)
                {
                    retVal = response.paging;

                    if (retVal.total > 0)
                    {
                        foreach(var ep in response.endpoints)
                        {
                            foreach (var reading in ep.readings)
                            {
                                //we will need to check for nulls once we have new Shadow endpoint
                                if (reading.no_flow_35_days != 0 || reading.reverse_flow_state != 0)
                                {
                                    var b = await UpdateEvent(ep, reading);                                   
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return retVal;
        }

        //this is just a test to see if I can insert into the new rds schema
        private static async Task<bool> UpdateEvent(Models.Response.ReadingsResponseEndpoint ep, Models.Response.ReadingsResponseEndpointReading er)
        {
            try
            {
                if (Program._mySqlConnection != null)
                {
                    if (Program._mySqlConnection.State != System.Data.ConnectionState.Open)
                        Program._mySqlConnection.Open();

                    var sql = $"INSERT INTO reverseflowevents (SiteId, MeterNumber, MiuId, MeterSize, AccountNumber, RouteId, AmrAmiValue, 35DayReverseFlowFag, 24HourReserveFlowFlag, StartDateUTC, LastReading, LastReadingDateUTC) VALUES('{ep.site_id}', '{ep.meter_number}', '{ep.miu_id}', '1', 'account1', 'route1', '1', '{er.no_flow_35_days}', '{er.reverse_flow_state}', '{er.reading_datetime}', '{er.reading}', '{er.reading_datetime}');";

                    using (var cmd = Program._mySqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sql;
                        var i = cmd.ExecuteNonQuery();
                        Console.WriteLine($"{i} rows affected");
                    }
                }
            }
            finally
            {
            }

            return await Task.FromResult(true);
        }

        //public static async Task<Models.Response.PagedReadingsResponse> CheckForEvents(Models.Response.EndpointsResponseEndpoint ep)
        //{
        //    Models.Response.PagedReadingsResponse retVal = new Models.Response.PagedReadingsResponse();

        //    var dtStart = DateTime.UtcNow.AddDays(-720);
        //    var dtEnd = dtStart.AddDays(1);

        //    //string apiRequest = string.Format(API_GET_SHADOW_READINGS_FOR_SITE, ep.site_id, dtStart.ToString(), dtEnd.ToString(), page);

        //    try
        //    {
        //        //var token = new System.Threading.CancellationTokenSource().Token;
        //        //retVal = await Program.ApiService.PutAsync<Models.Response.PagedReadingsResponse, Models.Request.ReadingsRequest>(
        //        //    API_SHADOW_READINGS, token, 0, request);

        //        //if (retVal != null)
        //        //{
        //        //    foreach (var reading in retVal.)
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }

        //    return retVal;
        //}


        //test
        //public static List<Models.TenantEndpoint> foo()
        //{
        //    List<Models.TenantEndpoint> lEndpoints = new List<Models.TenantEndpoint>();

        //    MySqlConnection cnn = new MySqlConnection(string.Format(Common.Strings.RDSConnections.DEV_TENANT, SITEID));

        //    try
        //    {
        //        cnn.Open();
        //        using (var cmd = cnn.CreateCommand())
        //        {
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.CommandText = "sp_GetEndpointEventData";

        //            using (var dataReader = cmd.ExecuteReader())
        //            {
        //                lEndpoints = Helpers.MySqlDataReaderMapToList(dataReader);
        //                if (lEndpoints != null)
        //                {
        //                    Console.WriteLine($"Endpoints:{lEndpoints.Count}");
        //                }
        //            }
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }

        //    return lEndpoints;
        //}


        static __AuthService _authService = null;

        public static __AuthService AuthService
        {
            get
            {
                if (_authService == null)
                {
                    _authService = new __AuthService();
                    _authService.Initialize(
                        IdpUrl,
                        Common.Strings.IDPClientID,
                        Common.Strings.IDPClientSecret,
                        Common.Strings.IDPClentScope);
                }
                return _authService;
            }

        }

        static Func<string> IdpUrl = () =>
        {
            return IDP;
        };


        static Func<string> ApiBaseUrl = () => {
            return Program.URI;
        };
    }
}
