﻿using System;
using System.Text;
using Newtonsoft.Json.Linq;
using MySql.Data;
using System.Threading.Tasks;
using System.Data;
using System.Collections.Generic;

namespace RFS_Backflow_Processor.Common
{
    public static class MySqlCommandExtensions
    {
        public static List<Models.TenantEndpoint> ExecuteMySqlStoredProcedure(this MySql.Data.MySqlClient.MySqlCommand cmd, string storedProcedure)
        {
            try
            {
                cmd.CommandText = storedProcedure;
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd.Connection.State != ConnectionState.Open)
                    cmd.Connection.Open();

                using (var dataReader = cmd.ExecuteReader())
                {
                    var test = Helpers.MySqlDataReaderMapToList(dataReader);
                    return test;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                    cmd.Dispose();
                }
            }
        }
    }

    public static class StringExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string SafeTrim(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;
            return s.Trim();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static string DecodeAuthToken(this string token)
        {
            var parts = token.Split('.');

            string partToConvert = parts[1];
            partToConvert = partToConvert.Replace('-', '+');
            partToConvert = partToConvert.Replace('_', '/');
            switch (partToConvert.Length % 4)
            {
                case 0:
                    break;
                case 2:
                    partToConvert += "==";
                    break;
                case 3:
                    partToConvert += "=";
                    break;
            }

            var partAsBytes = Convert.FromBase64String(partToConvert);
            var partAsUTF8String = Encoding.UTF8.GetString(partAsBytes, 0, partAsBytes.Length);

            return JObject.Parse(partAsUTF8String).ToString();
        }
    }

    public static class ExceptionExtensions
    {
        public static string TraceSummary(this Exception ex)
        {
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
            StringBuilder sb = new StringBuilder();
            sb.Append($"Name:{trace.GetFrame(0).GetMethod().ReflectedType.FullName}").Append(", ");
            sb.Append($"Line:{trace.GetFrame(0).GetFileLineNumber()}").Append(", ");
            sb.Append($"Message:{ex.Message}");
            return sb.ToString();
        }
    }
}