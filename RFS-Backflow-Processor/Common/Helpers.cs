﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using RFS_Backflow_Processor.Models;

namespace RFS_Backflow_Processor.Common
{
    public static class Helpers
    {
        public static List<TenantEndpoint> MySqlDataReaderMapToList(DbDataReader dr)
        {
            List<TenantEndpoint> list = new List<TenantEndpoint>();
            while (dr.Read())
            {
                var obj = Activator.CreateInstance<TenantEndpoint>();
                foreach (System.Reflection.PropertyInfo prop in obj.GetType().GetProperties())
                {
                    //  skip entity collection properties
                    if (prop.PropertyType.IsConstructedGenericType)
                    {
                        if (prop.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>))
                        {
                            if (prop.PropertyType.GenericTypeArguments[0] == typeof(TenantEndpoint)) continue;
                        }
                    }

                    if (prop.PropertyType == typeof(bool))
                    {
                        prop.SetValue(obj, Convert.ToBoolean(dr[prop.Name]), null);
                    }
                    else if (!Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                }
                list.Add(obj);
            }
            return list;
        }
    }
}
